/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/* inclusion guard */
#ifndef __AUM_COMMON_H__
#define __AUM_COMMON_H__

#define AUM_RUNTIME_DIR "/var/run/apertis-update-manager"

#endif /* __AUM_COMMON_H__ */
