/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/* inclusion guard */
#ifndef __AUM_OSTREE_UPGRADER_H__
#define __AUM_OSTREE_UPGRADER_H__

#include <glib-object.h>
#include <glib.h>
#include <gio/gio.h>

G_BEGIN_DECLS

typedef enum {
        AUM_OSTREE_UPGRADE_STATE_UNKNOWN = 0,
        AUM_OSTREE_UPGRADE_STATE_CHECKING,
        AUM_OSTREE_UPGRADE_STATE_DOWNLOADING,
        AUM_OSTREE_UPGRADE_STATE_DEPLOYING,
        AUM_OSTREE_UPGRADE_STATE_PENDING = 100,
        AUM_OSTREE_UPGRADE_STATE_UPTODATE = 200
} AumOstreeUpgradeState;

/*
 * Type declaration.
 */
#define AUM_TYPE_OSTREE_UPGRADER aum_ostree_upgrader_get_type ()
G_DECLARE_FINAL_TYPE (AumOstreeUpgrader,
                      aum_ostree_upgrader,
                      AUM,
                      OSTREE_UPGRADER,
                      GObject)

void aum_ostree_upgrader_set_active(AumOstreeUpgrader *self,
                                      gboolean active);

void
aum_ostree_upgrader_set_ready (AumOstreeUpgrader *self);

void aum_ostree_upgrade_apply_pending(AumOstreeUpgrader *self);

gboolean aum_ostree_upgrader_undeploy (AumOstreeUpgrader *self);

gboolean aum_ostree_upgrade_apply_static_delta(AumOstreeUpgrader *self,
                                               const gchar *filename);

gboolean
aum_ostree_upgrader_check_blacklisted(const gchar *filename,
                                      const gchar *checksum);
void
aum_ostree_upgrader_set_blacklisted(const gchar *filename,
                                    const gchar *checksum);

G_END_DECLS

#endif /* __AUM_OSTREE_UPGRADER_H__ */
