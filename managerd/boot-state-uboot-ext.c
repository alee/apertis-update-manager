/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/**
 * SECTION: bootstateubootext
 * @title: AumBootStateUbootExt
 * @short_description: Ostree upgrade handler
 * @include: boot-state.h
 *
 * The boot state is responsible for reseting the bootcount
 */

#include "boot-state-uboot-ext.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include <errno.h>

/* For u-boot */
#define BC_FILE  "/boot/uboot.cnt"
#define BC_MAGIC 0xbc

struct _AumBootStateUbootExt
{
  GObject parent;
  gint active;
  gchar *filename;
  int bootcount;
  guint bootlimit;
};

static void
aum_boot_state_uboot_ext_iface_init (AumBootStateInterface *iface);

G_DEFINE_TYPE_WITH_CODE (AumBootStateUbootExt, aum_boot_state_uboot_ext, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (AUM_TYPE_BOOT_STATE, aum_boot_state_uboot_ext_iface_init));

/**
 * @brief Destroy environment structure 
 *
 * @param state
 */
static void
aum_boot_state_uboot_ext_cleanup(AumBootStateUbootExt *state)
{
  g_clear_pointer(&state->filename, g_free);
}

/**
 * @brief Read bootcount from /boot/uboot.cnt
 *
 * @param state
 *
 * @return 
 */
static gboolean
aum_boot_state_uboot_ext_read(AumBootStateUbootExt *state)
{
  g_autofree gchar *contents = NULL;
  gsize length = 0;
  g_autoptr (GError) error=NULL;

  g_file_get_contents (state->filename, &contents, &length,
                       &error);
  if (error)
    goto err;

  if (length != 2)
    goto err;

  if (contents[0] != BC_MAGIC)
    goto err;

  state->bootcount = contents[1];

  return TRUE;

err:
  g_message ("%s: %s", __FUNCTION__, error ? error->message : "unknown error");
  return FALSE;
}

/**
 * @brief save bootcount to disk
 *
 * @param state
 *
 * @return 
 */
static gboolean
aum_boot_state_uboot_ext_save(AumBootStateUbootExt *state)
{
  g_autoptr (GError) error=NULL;
  gchar buffer[2] = { BC_MAGIC, state->bootcount & 0xFF };

  g_file_set_contents (state->filename, buffer,
                       sizeof(buffer),
                       &error);
  if (error)
    goto err;

  return TRUE;

err:
  g_critical ("%s: %s", __FUNCTION__, error ? error->message : "unknown error");
  return FALSE;
}

static gboolean
aum_boot_state_uboot_ext_is_active (AumBootState * boot_state)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT(boot_state);
  return state->active;
}

static const gchar *
aum_boot_state_uboot_ext_get_name (AumBootState * boot_state)
{
  return "UBoot environment external";
}

static void
aum_boot_state_uboot_ext_init (AumBootStateUbootExt * state)
{
  state->filename = g_strdup(BC_FILE);
  state->active = FALSE;

  if (aum_boot_state_uboot_ext_read(state) == FALSE)
    goto err;

  state->active = TRUE;

  g_message ("Loaded boot count from ext: count %d",
             state->bootcount);
  return;

err:
  g_critical ("Cannot initialize boot-state from U-Boot ext environment");
}

static gint
aum_boot_state_uboot_ext_get_boot_count (AumBootState * boot_state)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT (boot_state);
  return state->bootcount;
}

static gint
aum_boot_state_uboot_ext_get_boot_limit (AumBootState * boot_state)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT (boot_state);
  return  state->bootlimit;
}

static void
aum_boot_state_uboot_ext_set_boot_limit (AumBootState * boot_state, guint limit)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT (boot_state);
  state->bootlimit = limit;
}

static gboolean
aum_boot_state_uboot_ext_get_update_available (AumBootState * boot_state)
{
  return TRUE;
}

/**
 * @brief Trigger bootcount mechanism in U_Boot
 *
 * @param boot_state
 * @param update_available: set to true if an update has just been applied, and before rebooting, or false if the reboot was successful
 *
 * @return 
 */
static gboolean
aum_boot_state_uboot_ext_set_update_available (AumBootState * boot_state,
                                               gboolean update_available)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT (boot_state);
  state->bootcount = 0;
  return aum_boot_state_uboot_ext_save (state);
}

static void
aum_boot_state_uboot_ext_iface_init (AumBootStateInterface *iface)
{
  iface->is_active = aum_boot_state_uboot_ext_is_active;
  iface->get_name = aum_boot_state_uboot_ext_get_name;
  iface->get_boot_count = aum_boot_state_uboot_ext_get_boot_count;
  iface->get_boot_limit = aum_boot_state_uboot_ext_get_boot_limit;
  iface->set_boot_limit = aum_boot_state_uboot_ext_set_boot_limit;
  iface->get_update_available = aum_boot_state_uboot_ext_get_update_available;
  iface->set_update_available = aum_boot_state_uboot_ext_set_update_available;
}

static void
aum_boot_state_uboot_ext_dispose(GObject *obj)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT (obj);
  aum_boot_state_uboot_ext_cleanup(state);
}

static void
aum_boot_state_uboot_ext_class_init (AumBootStateUbootExtClass * cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);
  object_class->dispose = aum_boot_state_uboot_ext_dispose;
}
