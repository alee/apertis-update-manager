/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/**
 * SECTION: bootstateubootenv
 * @title: AumBootStateUbootEnv
 * @short_description: Ostree upgrade handler
 * @include: boot-state.h
 *
 * The boot state is responsible for reseting the bootcount
 */

#include "boot-state-uboot-env.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include <errno.h>

/* For SabreLite */
/* TODO: move to config file */
#define ENV_DEV "/dev/mmcblk0"
#define ENV_SIZE (16 * 512 - sizeof(guint32))
#define ENV_ADDR 0xC0000

struct _AumBootStateUbootEnv
{
  GObject parent;
  gint active;
  guint32 offset;
  guint32 datasize;
  gchar *filename;
  GHashTable *vars;
};


static void
aum_boot_state_uboot_env_iface_init (AumBootStateInterface *iface);

G_DEFINE_TYPE_WITH_CODE (AumBootStateUbootEnv, aum_boot_state_uboot_env, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (AUM_TYPE_BOOT_STATE, aum_boot_state_uboot_env_iface_init));

/**
 * @brief Destroy environment structure 
 *
 * @param env
 */
static void
aum_boot_state_uboot_env_cleanup(AumBootStateUbootEnv *env)
{
  g_hash_table_destroy(env->vars);
  env->vars = NULL;
  g_clear_pointer(&env->filename, g_free);
  env->offset = 0;
  env->datasize = 0;
}

/**
 * @brief Calculate checksum of buffer
 *
 * @param env
 */
static guint32
aum_boot_state_uboot_env_checksum(gchar *data, gsize size)
{
  guint32 crc = crc32(0L, Z_NULL, 0);
  if (data != NULL)
    {
      crc = crc32(crc, (guchar*)data, size);
    }
  g_debug("Calculated checksum: 0x%X", crc);
  return crc;
}

/**
 * @brief Parse buffer and put k:v pairs to env->vars
 *
 * @param env
 */
static gboolean
aum_boot_state_uboot_env_parse(AumBootStateUbootEnv *env, gchar *data)
{
  gchar *name = NULL;
  gchar *value = NULL;
  gchar **var = NULL;
  char *cur = NULL;
  size_t tail = env->datasize;
  size_t len;

  cur = data;

  do
    {
      len = strnlen (cur, tail);
      /* Check if we "read" a 0-ended string
       * Or this is the end of buffer but not the end of environment */
      if (tail == len || *(cur + len) != 0 )
        goto err;

      g_debug("Read U-Boot env: %s", cur);

      /* Add var into vars list */
      var = g_strsplit( cur, "=", 2);
      if(var[0] == NULL || var[1] == NULL)
        {
          g_strfreev(var);
          goto err;
        }
      name = g_strdup (var[0]);
      value = g_strdup (var[1]);
      g_strfreev(var);

      g_hash_table_insert(env->vars, name, value);

      len++;
      tail -= len;
      cur += len;

    } while (*cur != 0); /* End of environment detected */

  return TRUE;

err:
  g_critical("Can't parse the U-boot environment");
  return FALSE;
}


/**
 * @brief Read 
 *
 * @param env
 *
 * @return 
 */
static gboolean
aum_boot_state_uboot_env_read(AumBootStateUbootEnv *env)
{
  g_autofree gchar *data = NULL;
  gchar *buf = NULL;
  gsize len=0, bufsize=0;
  guint32 crc=0;

  g_autoptr (GIOChannel) storage = NULL;
  g_autoptr (GError) error=NULL;

  GIOStatus status;

  storage = g_io_channel_new_file (env->filename, "r", &error);
  if (error)
    goto err;

  bufsize = env->datasize + sizeof(guint32);

  data = (gchar *) g_malloc0(bufsize);
  if (data == NULL)
    goto err;
  buf = data;

  g_io_channel_set_encoding (storage, NULL, NULL);
  status = g_io_channel_seek_position (storage, env->offset, G_SEEK_SET, &error); 
  if (status != G_IO_STATUS_NORMAL)
    goto err;

  do
    {
      status = g_io_channel_read_chars (storage, buf, bufsize, &len, &error);
      switch(status)
        {
        case G_IO_STATUS_NORMAL:
          break;
        case G_IO_STATUS_AGAIN:
          continue;
        default:
          goto err;
        }
      bufsize -= len;
      buf += len;
    }
  while (bufsize);

  g_debug("Read checksum: 0x%X", *(guint*)data);

  crc = aum_boot_state_uboot_env_checksum (data + sizeof(guint32), env->datasize);
  if ( crc != *(guint32*)data)
    {
      g_critical("Wrong checksum: 0x%X", *(guint*)data);
      goto err;
    }

  return aum_boot_state_uboot_env_parse(env, data+sizeof(guint32));

err:
  g_message ("%s: %s", __FUNCTION__, error ? error->message : "unknown error");
  return FALSE;
}


/**
 * @brief Serialize variables
 *
 * After the work of function:
 * buffer with all variables to be written to storage
 *
 * @param env
 *
 * @return 
 */
static gchar *
aum_boot_state_uboot_env_serialize(AumBootStateUbootEnv *env)
{
  g_autofree gchar *data = NULL;
  GHashTableIter ihash;
  gchar *variable, *value;
  gchar *cur;
  size_t tail = env->datasize;
  size_t len;
  g_autoptr (GSList) list = NULL;
  GSList *ilist = NULL;

  data = (gchar *) g_malloc0(env->datasize);
  if (data == NULL)
    goto err;
  cur = data;

  /* Need to write a sorted data */
  g_hash_table_iter_init (&ihash, env->vars);
  while(g_hash_table_iter_next (&ihash, (gpointer) &variable, (gpointer) &value))
    list = g_slist_insert_sorted (list, variable, (GCompareFunc) g_strcmp0);

  for (ilist = list; ilist; ilist = ilist->next)
    {
      variable = (gchar *) ilist->data;
      value = (gchar *) g_hash_table_lookup( env->vars, variable);

      g_debug("Variable in memory: %s = %s", (char *)ilist->data, value);

      /* Check if we have enough space to handle variables and double \0 */
      len = g_snprintf(cur, tail, "%s=%s", variable, value);
      len++;
      tail -= len;
      /* Need to have enough space for additional \0 at the end of buffer */
      if(tail < 1)
        goto err;
      cur += len;
    }

  return g_steal_pointer(&data);

err:
  g_slist_free(list);
  return NULL;
}


/**
 * @brief Write some data to channel
 *
 * @param chan
 * @param buf
 * @param size
 * @param error
 *
 * @return
 */
static gboolean
aum_boot_state_uboot_env_write_buf(GIOChannel *chan, gchar *buf, guint32 size)
{
  g_autoptr (GError) error=NULL;
  GIOStatus status;
  gsize len=0;

  do
    {
      status = g_io_channel_write_chars (chan, buf, size, &len, &error);
      switch(status)
        {
        case G_IO_STATUS_NORMAL:
          break;
        case G_IO_STATUS_AGAIN:
          /* Burn CPU here */
          continue;
        default:
          goto err;
        }
      buf+= len;
      size -= len;
    }
  while (size);

  return TRUE;
err:
  g_critical ("%s: %s", __FUNCTION__, error ? error->message : "unknown error");
  return FALSE;
}

/**
 * @brief 
 *
 * @param env
 *
 * @return 
 */
static gboolean
aum_boot_state_uboot_env_save(AumBootStateUbootEnv *env)
{
  g_autofree gchar *data=NULL;
  guint32 crc=0;

  g_autoptr (GIOChannel) storage = NULL;
  g_autoptr (GError) error=NULL;

  data = aum_boot_state_uboot_env_serialize(env);
  if (data == NULL)
    goto err;
  storage = g_io_channel_new_file (env->filename, "r+", &error);
  if (error)
    goto err;

  g_io_channel_set_encoding (storage, NULL, NULL);
  g_io_channel_seek_position (storage, env->offset, G_SEEK_SET, &error);
  if (error)
    goto err;

  /* TODO: check LE/BE */
  crc = aum_boot_state_uboot_env_checksum (data, env->datasize);

  if (! aum_boot_state_uboot_env_write_buf (storage, (gchar *) &crc, sizeof(guint32)))
    goto err;

  if (! aum_boot_state_uboot_env_write_buf (storage, data, env->datasize))
    goto err;

  /* Explicitly write the buffer */
  while (g_io_channel_flush (storage, &error) == G_IO_STATUS_AGAIN);
  if (error)
    goto err;

  return TRUE;

err:
  g_critical ("%s: %s", __FUNCTION__, error ? error->message : "unknown error");
  return FALSE;
}

static gboolean
aum_boot_state_uboot_env_is_active (AumBootState * boot_state)
{
  AumBootStateUbootEnv *env = AUM_BOOT_STATE_UBOOT_ENV(boot_state);
  return env->active;
}

static const gchar *
aum_boot_state_uboot_env_get_name (AumBootState * boot_state)
{
  return "UBoot environment";
}

static void
aum_boot_state_uboot_env_init (AumBootStateUbootEnv * env)
{
  gchar *value = NULL;
  gint count=0, limit=4;
  gboolean update_available=FALSE;

  env->vars = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
  env->filename = g_strdup(ENV_DEV);
  env->offset = ENV_ADDR;
  env->datasize = ENV_SIZE;
  env->active = FALSE;

  if (aum_boot_state_uboot_env_read(env) == FALSE)
    goto err;


  value = (gchar *) g_hash_table_lookup( env->vars, "bootlimit");
  if (value)
    limit = g_ascii_strtoll(value, NULL, 10);

  value = (gchar *) g_hash_table_lookup( env->vars, "bootcount");
  if (value)
    count = g_ascii_strtoll(value, NULL, 10);

  value = (gchar *) g_hash_table_lookup( env->vars, "upgrade_available");
  if (value)
    update_available = TRUE;

  env->active = TRUE;

  g_message ("Loaded boot state: count %d, limit %d, update_available %d",
             count, limit, update_available);
  return;

err:
  g_critical ("Cannot initialize boot-state from U-Boot environment");

}

gint
aum_boot_state_uboot_env_get_int (AumBootState * boot_state, const gchar *var)
{
  AumBootStateUbootEnv *env = AUM_BOOT_STATE_UBOOT_ENV (boot_state);
  gchar *value = (gchar *) g_hash_table_lookup( env->vars, var);
  return value ? (gint) g_ascii_strtoll(value, NULL, 10): 0;
}

static gint
aum_boot_state_uboot_env_get_boot_count (AumBootState * boot_state)
{
  return aum_boot_state_uboot_env_get_int (boot_state, "bootcount");
}

static gint
aum_boot_state_uboot_env_get_boot_limit (AumBootState * boot_state)
{
  return aum_boot_state_uboot_env_get_int (boot_state, "bootlimit");
}

static gboolean
aum_boot_state_uboot_env_get_update_available (AumBootState * boot_state)
{
  gint var = aum_boot_state_uboot_env_get_int (boot_state, "upgrade_available");
  return var ? TRUE : FALSE;
}

/**
 * @brief Trigger bootcount mechanism in U_Boot
 *
 * @param boot_state
 * @param update_available: set to true if an update has just been applied, and before rebooting, or false if the reboot was successful
 *
 * @return 
 */
static gboolean
aum_boot_state_uboot_env_set_update_available (AumBootState * boot_state,
                                               gboolean update_available)
{
  AumBootStateUbootEnv *env = AUM_BOOT_STATE_UBOOT_ENV (boot_state);

  if (update_available)
    {
      g_hash_table_insert (env->vars, g_strdup("upgrade_available"), g_strdup("1"));
    }
  else
    {
      g_hash_table_remove (env->vars, "upgrade_available");
    }
  g_hash_table_insert (env->vars, g_strdup("bootcount"), g_strdup("0"));
  return aum_boot_state_uboot_env_save (env);
}


static void
aum_boot_state_uboot_env_iface_init (AumBootStateInterface *iface)
{
  iface->is_active = aum_boot_state_uboot_env_is_active;
  iface->get_name = aum_boot_state_uboot_env_get_name;
  iface->get_boot_count = aum_boot_state_uboot_env_get_boot_count;
  iface->get_boot_limit = aum_boot_state_uboot_env_get_boot_limit;
  iface->get_update_available = aum_boot_state_uboot_env_get_update_available;
  iface->set_update_available = aum_boot_state_uboot_env_set_update_available;
}

static void
aum_boot_state_uboot_env_dispose(GObject *obj)
{
  AumBootStateUbootEnv *env = AUM_BOOT_STATE_UBOOT_ENV (obj);
  aum_boot_state_uboot_env_cleanup(env);
}

static void
aum_boot_state_uboot_env_class_init (AumBootStateUbootEnvClass * cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);
  object_class->dispose = aum_boot_state_uboot_env_dispose;
}
