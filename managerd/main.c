/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */


#include "common.h"
#include "ostree-upgrader.h"
#include "boot-state.h"

#include "apertis-update-manager-dbus.h"

#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include <systemd/sd-daemon.h>

#define DEFAULT_BOOTLIMIT 4

typedef struct
{
  GNetworkMonitor *monitor;
  gboolean online;
  GMainLoop *loop;
  AumOstreeUpgrader *upgrader;
  gchar *boot_state_type;
  guint boot_limit;
  AumBootState *boot_state;
  GVolumeMonitor *volumes;

  GDBusConnection *bus;
  aumApertisUpdateManager *aum_dbus;

  GQueue upgrade_handlers;

  gboolean resetting_system;
  GQueue pending_reset_jobs;
} AUM;

typedef struct
{
  AUM *aum;
  gchar *name;
  guint watch_id;
} UpgradeHandler;

static void
handler_vanished (GDBusConnection *connection,
                  const gchar *name,
                  gpointer user_data)
{
  UpgradeHandler *handler = user_data;

  g_queue_remove (&handler->aum->upgrade_handlers, handler);

  g_bus_unwatch_name (handler->watch_id);
  g_free (handler->name);
  g_slice_free (UpgradeHandler, handler);
}

static void
aum_add_upgrade_handler (AUM *aum, const gchar *name)
{
  UpgradeHandler *handler = g_slice_new0 (UpgradeHandler);

  handler->aum = aum;
  handler->name = g_strdup (name);
  handler->watch_id = g_bus_watch_name_on_connection (aum->bus,
                                                      handler->name,
                                                      G_BUS_NAME_WATCHER_FLAGS_NONE,
                                                      NULL,
                                                      handler_vanished,
                                                      handler,
                                                      NULL);

  g_queue_push_head (&aum->upgrade_handlers, handler);
}

static void
aum_update_network_status (AUM * aum)
{
  gboolean active;
  gboolean online = g_network_monitor_get_network_available (aum->monitor);

  /* GNetworkMonitor seems to signal changes somewhat spuriously so filter out
   * the redundant ones */
  if (online == aum->online)
    return;

  g_message ("Network status: %s", online ? "online" : "offline");

  aum->online = online;
  g_object_set (aum->aum_dbus, "network-connected", online, NULL);
  g_object_get (aum->aum_dbus, "updates-from-network", &active, NULL);

  aum_ostree_upgrader_set_active (aum->upgrader, online && active);
}

static void
upgrade_state_changed (AumOstreeUpgrader *upgrader,
                       GParamSpec *pspec,
                       gpointer user_data)
{
  AUM *aum = user_data;
  AumOstreeUpgradeState state;

  g_object_get (upgrader, "upgrade-state", &state, NULL);
  if (state == AUM_OSTREE_UPGRADE_STATE_PENDING)
    {
      aum_boot_state_set_update_available (aum->boot_state, TRUE);

      if (!aum->resetting_system
          && g_queue_get_length (&aum->upgrade_handlers) == 0)
        {
          aum_ostree_upgrade_apply_pending (upgrader);
        }
      else
        {
          g_message ("Upgrade handlers registered or the system is being reset, not rebooting");
        }
    }
}

static void
network_monitor_changed (GNetworkMonitor * monitor,
                         gboolean available, gpointer user_data)
{
  AUM *aum = user_data;

  aum_update_network_status (aum);
}

static void
mount_added (GVolumeMonitor * volumem,
             GMount *mount, gpointer user_data)
{
  AUM *aum = user_data;
  g_autoptr (GFile) location = NULL;
  g_autofree gchar *path = NULL;
  g_autofree gchar *filename = NULL;

  if (aum->resetting_system)
    return;

  location = g_mount_get_default_location (mount);
  path = g_file_get_path (location);

  g_message ("mount added : %s", path);

  /* TODO: clarify future naming, pattern matching? */
  filename = g_build_filename(path, "static-update.bundle", NULL);

  if (!g_file_test (filename, G_FILE_TEST_EXISTS))
    return;

  aum_ostree_upgrade_apply_static_delta (aum->upgrader, filename);
}

static gboolean
setup_runtime_dir(void)
{

  if (!g_file_test (AUM_RUNTIME_DIR, G_FILE_TEST_IS_DIR))
    {
      int ret;
      ret = g_mkdir(AUM_RUNTIME_DIR, 0700);

      if (ret != 0)
        {
          g_message ("Couldn't create runtime directory, Exiting...");
          return FALSE;
        }
    }
  else
    {
      g_autoptr (GError) error = NULL;
      g_autoptr (GDir) dir = NULL;

      dir = g_dir_open (AUM_RUNTIME_DIR, 0, &error);
      if (dir == NULL)
        {
          g_message ("Failed to open runtime directory, Exiting...");
          return FALSE;
        }
      for (const gchar *n = g_dir_read_name (dir);
           n != NULL;
           n = g_dir_read_name (dir))
        {
          g_autofree gchar *p = g_build_filename (AUM_RUNTIME_DIR, n, NULL);
          g_unlink (p);
        }
    }

  return TRUE;
}

static gboolean
handle_register_system_upgrade_handler (aumApertisUpdateManager *dbus,
                                        GDBusMethodInvocation *invocation,
                                        gpointer user_data)
{
  AUM *aum = user_data;

  aum_add_upgrade_handler (aum,
                           g_dbus_method_invocation_get_sender (invocation));

  g_dbus_method_invocation_return_value (invocation, NULL);

  return TRUE;
}

static gboolean
handle_apply_system_upgrade (aumApertisUpdateManager *dbus,
                             GDBusMethodInvocation *invocation,
                             gpointer user_data)
{
  AUM *aum = user_data;

  if (!aum->resetting_system)
    {
      aum_ostree_upgrade_apply_pending (aum->upgrader);
    }
  g_dbus_method_invocation_return_value (invocation, NULL);

  return TRUE;
}

static gboolean
mark_update_successful(AUM *aum)
{
  gboolean result = FALSE;

  if (!aum_boot_state_get_update_available (aum->boot_state))
    return TRUE;

  if (boot_state_check_boot_successful(aum->boot_state))
    {
      g_message ("Marked update as successful");
      result = TRUE;
    }
  else
    {
      g_message ("Undeploying");
      result = aum_ostree_upgrader_undeploy (aum->upgrader);
    }

  if (result)
    result = aum_boot_state_set_update_available (aum->boot_state, FALSE);

  return result;
}

static gboolean
read_configuration_file (AUM *aum)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GKeyFile) key_file = g_key_file_new ();
  gboolean updates_from_network = FALSE;

  aum->boot_state_type = NULL;
  aum->boot_limit = DEFAULT_BOOTLIMIT;

  key_file = g_key_file_new ();
  if (!g_key_file_load_from_file (key_file, "/etc/apertis-update-manager.ini", G_KEY_FILE_NONE, &error))
    goto err;

  if (g_key_file_has_key (key_file, "Manager", "UpdatesFromNetwork", NULL))
    updates_from_network = g_key_file_get_boolean (key_file,
                                                   "Manager", "UpdatesFromNetwork",
                                                   NULL);

  if (g_key_file_has_key (key_file, "Manager", "UpdatesFromNetwork", NULL))
    aum->boot_state_type = g_key_file_get_string (key_file,
                                                  "Manager", "BootState",
                                                  NULL);

  if (g_key_file_has_key (key_file, "Manager", "BootLimit", NULL))
    aum->boot_limit = g_key_file_get_integer (key_file,
                                              "Manager", "BootLimit",
                                              NULL);

  g_object_set (aum->aum_dbus, "updates-from-network", updates_from_network, NULL);

  return TRUE;

err:
  g_warning ("Could not load configuration file: %s", error->message);
  return FALSE;
}

static gboolean
handle_mark_update_successful (aumApertisUpdateManager *dbus,
                             GDBusMethodInvocation *invocation,
                             gpointer user_data)
{
  AUM *aum = user_data;

  if (aum->resetting_system)
    g_dbus_method_invocation_return_dbus_error (invocation,
                                                "org.apertis.ApertisUpdateManager",
                                                "System is being reset");
  else if (!mark_update_successful (aum))
    g_dbus_method_invocation_return_dbus_error (invocation,
                                                "org.apertis.ApertisUpdateManager",
                                                "Failed to mark update successful");
  else
    g_dbus_method_invocation_return_value (invocation, NULL);

  return TRUE;
}

static gboolean
handle_apply_static_delta (aumApertisUpdateManager *dbus,
                           GDBusMethodInvocation *invocation,
                           gchar *filename,
                           gpointer user_data)
{
  AUM *aum = user_data;
  g_autoptr (GError) error = NULL;

  if (aum->resetting_system)
    g_dbus_method_invocation_return_dbus_error (invocation,
                                                "org.apertis.ApertisUpdateManager",
                                                "System is being reset");
  else if (!aum_ostree_upgrade_apply_static_delta (aum->upgrader, filename))
    g_dbus_method_invocation_return_dbus_error (invocation,
                                                "org.apertis.ApertisUpdateManager",
                                                "Failed to apply static delta");
  else
    g_dbus_method_invocation_return_value (invocation, NULL);

  return TRUE;
}

static gboolean
handle_check_network_updates (aumApertisUpdateManager *dbus,
                              GDBusMethodInvocation *invocation,
                              gpointer user_data)
{
  AUM *aum = user_data;

  if (aum->resetting_system)
    {
      g_dbus_method_invocation_return_dbus_error (invocation,
                                                  "org.apertis.ApertisUpdateManager",
                                                  "System is being reset");
      goto out;
    }

  /* Set the property to TRUE allowing online updates */
  aum_apertis_update_manager_set_updates_from_network (aum->aum_dbus, TRUE);

  /* Start server(s) polling */
  aum_ostree_upgrader_set_ready (aum->upgrader);
  aum_ostree_upgrader_set_active (aum->upgrader, aum->online);

  g_dbus_method_invocation_return_value (invocation, NULL);

out:
  return TRUE;
}

static void
updates_from_network_changed (aumApertisUpdateManager *dbus,
                              GParamSpec *pspec,
                              gpointer user_data)
{
  AUM *aum = user_data;
  gboolean active, online;

  g_object_get (dbus, "updates-from-network", &active, NULL);
  g_object_get (dbus, "network-connected", &online, NULL);

  g_message ("Auto update status: %s", active ? "active" : "disabled");

  aum_ostree_upgrader_set_active (aum->upgrader, online && active);
}

static void
bus_acquired_callback (GDBusConnection *connection,
                       const gchar *name,
                       gpointer user_data)
{
  AUM *aum = user_data;
  g_autoptr (GError) error = NULL;
  gboolean active;

  aum->bus = g_object_ref (connection);
  aum->aum_dbus = aum_apertis_update_manager_skeleton_new ();

  g_signal_connect (aum->aum_dbus, "handle-register-system-upgrade-handler",
                    G_CALLBACK (handle_register_system_upgrade_handler),
                    aum);

  g_signal_connect (aum->aum_dbus, "handle-apply-system-upgrade",
                    G_CALLBACK (handle_apply_system_upgrade),
                    aum);

  g_signal_connect (aum->aum_dbus, "handle-mark-update-successful",
                    G_CALLBACK (handle_mark_update_successful),
                    aum);

  g_signal_connect (aum->aum_dbus, "handle-apply-static-delta",
                    G_CALLBACK (handle_apply_static_delta),
                    aum);

  g_signal_connect (aum->aum_dbus, "handle-check-network-updates",
                    G_CALLBACK (handle_check_network_updates),
                    aum);

  read_configuration_file (aum);

  g_object_get (aum->aum_dbus, "updates-from-network", &active, NULL);
  g_message ("Auto update status: %s", active ? "active" : "disabled");
  g_signal_connect (aum->aum_dbus, "notify::updates-from-network",
                    G_CALLBACK (updates_from_network_changed),
                    aum);

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (aum->aum_dbus),
                                         connection, "/", &error))
    {
      g_warning ("Failed to export dbus interface: %s", error->message);
      g_main_loop_quit (aum->loop);
    }
}

static void
name_acquired_callback (GDBusConnection *connection,
                        const gchar *name,
                        gpointer user_data)
{
  AUM *aum = user_data;

  /* Fully setup lets go to work */
  aum->monitor = g_network_monitor_get_default ();
  aum->upgrader = g_object_new (AUM_TYPE_OSTREE_UPGRADER, NULL);

  if (aum->boot_state_type) {
    aum->boot_state = boot_state_factory_get_by_type(aum->boot_state_type);
  } else {
    aum->boot_state = boot_state_factory_get_active();
  }

  aum_boot_state_set_boot_limit (aum->boot_state, aum->boot_limit);

  aum->volumes = g_volume_monitor_get();

  g_signal_connect (aum->upgrader, "notify::upgrade-state",
                    G_CALLBACK (upgrade_state_changed), aum);

  g_object_bind_property (aum->upgrader, "upgrade-state",
                          aum->aum_dbus, "system-upgrade-state",
                          G_BINDING_DEFAULT | G_BINDING_SYNC_CREATE);

  g_signal_connect (aum->monitor, "network-changed",
                    G_CALLBACK (network_monitor_changed), aum);

  g_signal_connect (aum->volumes, "mount-added",
                    G_CALLBACK (mount_added), aum);

  aum_update_network_status (aum);
}

static void
name_lost_handler (GDBusConnection *connection,
                   const gchar *name,
                   gpointer user_data)
{
  AUM *aum = user_data;

  g_warning ("Failed to get bus name %s", name);
  g_main_loop_quit (aum->loop);
}

int
main (int argc, char **argv)
{
  AUM *aum;
  g_autoptr (GError) error = NULL;

  if (!setup_runtime_dir())
    return -1;

  aum = g_new0 (AUM, 1);
  g_queue_init (&aum->pending_reset_jobs);

  aum->loop = g_main_loop_new (NULL, FALSE);
  g_bus_own_name (G_BUS_TYPE_SYSTEM,
                  "org.apertis.ApertisUpdateManager",
                  G_BUS_NAME_OWNER_FLAGS_NONE,
                  bus_acquired_callback,
                  name_acquired_callback,
                  name_lost_handler,
                  aum,
                  NULL);

  g_main_loop_run (aum->loop);

  if (aum->monitor)
    g_object_unref (aum->monitor);

  if (aum->volumes)
    g_object_unref (aum->volumes);

  if (aum->upgrader)
    g_object_unref (aum->upgrader);

  if (aum->boot_state)
    g_object_unref (aum->boot_state);

  if (aum->aum_dbus)
    g_object_unref (aum->aum_dbus);

  if (aum->bus)
    g_object_unref (aum->bus);

  g_clear_pointer (&aum->boot_state_type, g_free);

  g_main_loop_unref (aum->loop);
  g_free (aum);

  return 0;
}
