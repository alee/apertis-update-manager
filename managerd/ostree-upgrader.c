
/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright (C) 2011 Colin Walters <walters@verbum.org>
 * Copyright (C) 2015 Red Hat, Inc.
 * Copyright © 2017,2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/**
 * SECTION: ostreeupgrader
 * @title: AumOstreeUpgrader
 * @short_description: Ostree upgrade handler
 * @include: ostree-upgrader.h
 *
 * The ostree upgrader is responsible for polling the remote ostree repository
 * and upgraded the system whenever a new deployment is available.
 */

#include "ostree-upgrader.h"
#include "managerd-enums.h"

#include <ostree.h>
#include <ostree-sign.h>
#include <errno.h>

#define OSTREE_STATIC_DELTA_META_ENTRY_FORMAT "(uayttay)"
#define OSTREE_STATIC_DELTA_FALLBACK_FORMAT "(yaytt)"
#define OSTREE_STATIC_DELTA_SUPERBLOCK_FORMAT "(a{sv}tayay" \
	OSTREE_COMMIT_GVARIANT_STRING "aya" \
	OSTREE_STATIC_DELTA_META_ENTRY_FORMAT \
	"a" OSTREE_STATIC_DELTA_FALLBACK_FORMAT ")"

#define POLL_SECONDS 300

#define BLACKLIST "/var/aum_blacklist.conf"

enum
{
  OSTREE_UPGRADE_FAILED = -1,
  OSTREE_UPGRADE_NO_UPDATE,
  OSTREE_UPGRADE_READY,
};

enum
{
  PROP_UPGRADE_STATE = 1,
  NUM_PROPERTIES
};

enum
{
  DELTA_METADATA = 0,
  DELTA_TIMESTAMP,
  DELTA_CHECKSUM_FROM,
  DELTA_CHECKSUM_TO,
  DELTA_COMMIT,
  DELTA_ARRAY_FROM_TO,
  DELTA_HEADERS,
  DELTA_FALLBACK
};

enum
{
  COMMIT_METADATA = 0,
  COMMIT_PARENT_CHECKSUM,
  COMMIT_RELATED,
  COMMIT_SUBJECT,
  COMMIT_BODY,
  COMMIT_TIMESTAMP,
  COMMIT_ROOT_TREE,
  COMMIT_ROOT_TREE_METADATA
};

struct _AumOstreeUpgrader
{
  GObject parent;
  gboolean active;
  gboolean updating;
  guint timeout_id;
  OstreeSysroot *sysroot;
  gchar *static_delta;

  AumOstreeUpgradeState upgrade_state;
};

static void
aum_ostree_upgrader_init (AumOstreeUpgrader * self)
{
  g_autoptr (GError) error = NULL;

  self->sysroot = ostree_sysroot_new_default ();

  if (!ostree_sysroot_load (self->sysroot, NULL, &error))
    {
      g_warning ("Ostree failed to load sysroot: %s", error->message);
      g_warning ("No Ostree updates will be done");
      g_object_unref (self->sysroot);
      self->sysroot = NULL;
    }
}

static void
aum_ostree_upgrader_dispose (GObject * object)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (object);

  if (self->timeout_id > 0)
    {
      g_source_remove (self->timeout_id);
    }

  if (self->sysroot)
    {
      g_object_unref (self->sysroot);
    }

  g_free (self->static_delta);
}

static void
aum_ostree_upgrader_get_property (GObject *object,
                                  guint prop_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (object);

  switch (prop_id)
    {
      case PROP_UPGRADE_STATE:
        g_value_set_enum (value, self->upgrade_state);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}


static GParamSpec *props[NUM_PROPERTIES] = { NULL, };

static void
aum_ostree_upgrader_class_init (AumOstreeUpgraderClass * cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  props[PROP_UPGRADE_STATE] =
    g_param_spec_enum ("upgrade-state",
                       "Upgrade State",
                       "The current upgrade state",
                       AUM_TYPE_OSTREE_UPGRADE_STATE,
                       AUM_OSTREE_UPGRADE_STATE_UNKNOWN,
                       G_PARAM_READABLE);

  object_class->get_property = aum_ostree_upgrader_get_property;
  object_class->dispose = aum_ostree_upgrader_dispose;

  g_object_class_install_properties (object_class,
                                     NUM_PROPERTIES,
                                     props);
}

static void _ostree_upgrader_start_update (AumOstreeUpgrader * self);

static void
_ostree_upgrader_set_upgrade_state (AumOstreeUpgrader * self,
                                    AumOstreeUpgradeState state)
{
  if (state != self->upgrade_state)
    {
      self->upgrade_state = state;
      g_object_notify_by_pspec (G_OBJECT (self), props[PROP_UPGRADE_STATE]);
    }
}

gboolean
_ostree_upgrader_poll_timeout_cb (gpointer user_data)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (user_data);

  self->timeout_id = 0;
  _ostree_upgrader_start_update (self);

  return G_SOURCE_REMOVE;
}

static void
_ostree_upgrader_update_cb (GObject * object,
                            GAsyncResult * res, gpointer user_data)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (object);

  g_autoptr (GError) error = NULL;
  gssize ret;

  ret = g_task_propagate_int (G_TASK (res), &error);

  switch (ret)
    {
    case OSTREE_UPGRADE_FAILED:
      g_message ("Ostree upgrade failed: %s", error->message);
      _ostree_upgrader_set_upgrade_state (self,
                                          AUM_OSTREE_UPGRADE_STATE_UNKNOWN);

      break;
    case OSTREE_UPGRADE_NO_UPDATE:
      g_message ("Ostree already up to date");
      _ostree_upgrader_set_upgrade_state (self,
                                          AUM_OSTREE_UPGRADE_STATE_UPTODATE);
      break;
    case OSTREE_UPGRADE_READY:
      g_message ("Ostree upgrade ready, system should be rebooted");
      _ostree_upgrader_set_upgrade_state (self,
                                          AUM_OSTREE_UPGRADE_STATE_PENDING);
      break;
    }

  g_object_unref (res);
  self->updating = FALSE;
  g_clear_pointer (&self->static_delta, g_free);

  if (self->active && self->upgrade_state != AUM_OSTREE_UPGRADE_STATE_PENDING)
    {
      self->timeout_id = g_timeout_add_seconds (POLL_SECONDS,
                                                _ostree_upgrader_poll_timeout_cb,
                                                self);
    }
  g_object_unref (self);
}

/* Helper copies from ostree source */

/**
 * _formatted_time_remaining_from_seconds
 * @seconds_remaining: Estimated number of seconds remaining.
 *
 * Returns a strings showing the number of days, hours, minutes
 * and seconds remaining.
 **/
static char *
_formatted_time_remaining_from_seconds (guint64 seconds_remaining)
{
  guint64 minutes_remaining = seconds_remaining / 60;
  guint64 hours_remaining = minutes_remaining / 60;
  guint64 days_remaining = hours_remaining / 24;

  GString *description = g_string_new (NULL);

  if (days_remaining)
    g_string_append_printf (description, "%" G_GUINT64_FORMAT " days ",
                            days_remaining);

  if (hours_remaining)
    g_string_append_printf (description, "%" G_GUINT64_FORMAT " hours ",
                            hours_remaining % 24);

  if (minutes_remaining)
    g_string_append_printf (description, "%" G_GUINT64_FORMAT " minutes ",
                            minutes_remaining % 60);

  g_string_append_printf (description, "%" G_GUINT64_FORMAT " seconds ",
                          seconds_remaining % 60);

  return g_string_free (description, FALSE);
}

/* Adapted from ostree source function
 * ostree_repo_pull_default_console_progress_changed
 */
static void
_ostree_upgrader_log_progress (OstreeAsyncProgress * progress,
                               gpointer user_data)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (user_data);
  g_autofree char *status = NULL;
  gboolean scanning;
  guint outstanding_fetches;
  guint outstanding_metadata_fetches;
  guint outstanding_writes;
  guint n_scanned_metadata;
  guint fetched_delta_parts;
  guint total_delta_parts;
  guint fetched_delta_part_fallbacks;
  guint total_delta_part_fallbacks;

  g_autoptr (GString) buf = g_string_new ("");

  _ostree_upgrader_set_upgrade_state (self,
                                      AUM_OSTREE_UPGRADE_STATE_DOWNLOADING);

  ostree_async_progress_get (progress,
                             "outstanding-fetches", "u", &outstanding_fetches,
                             "outstanding-metadata-fetches", "u",
                             &outstanding_metadata_fetches,
                             "outstanding-writes", "u", &outstanding_writes,
                             "scanning", "u", &scanning, "scanned-metadata",
                             "u", &n_scanned_metadata, "fetched-delta-parts",
                             "u", &fetched_delta_parts, "total-delta-parts",
                             "u", &total_delta_parts,
                             "fetched-delta-fallbacks", "u",
                             &fetched_delta_part_fallbacks,
                             "total-delta-fallbacks", "u",
                             &total_delta_part_fallbacks, "status", "s",
                             &status, NULL);

  if (*status != '\0')
    {
      g_string_append (buf, status);
    }
  else if (outstanding_fetches)
    {
      guint64 bytes_transferred, start_time, total_delta_part_size;
      guint fetched, metadata_fetched, requested;
      guint64 current_time = g_get_monotonic_time ();
      g_autofree char *formatted_bytes_transferred = NULL;
      g_autofree char *formatted_bytes_sec = NULL;
      guint64 bytes_sec;

      /* Note: This is not atomic wrt the above getter call. */
      ostree_async_progress_get (progress,
                                 "bytes-transferred", "t", &bytes_transferred,
                                 "fetched", "u", &fetched,
                                 "metadata-fetched", "u", &metadata_fetched,
                                 "requested", "u", &requested,
                                 "start-time", "t", &start_time,
                                 "total-delta-part-size", "t",
                                 &total_delta_part_size, NULL);

      formatted_bytes_transferred = g_format_size_full (bytes_transferred, 0);

      /* Ignore the first second, or when we haven't transferred any
       * data, since those could cause divide by zero below.
       */
      if ((current_time - start_time) < G_USEC_PER_SEC ||
          bytes_transferred == 0)
        {
          bytes_sec = 0;
          formatted_bytes_sec = g_strdup ("-");
        }
      else
        {
          bytes_sec =
            bytes_transferred / ((current_time - start_time) /
                                 G_USEC_PER_SEC);
          formatted_bytes_sec = g_format_size (bytes_sec);
        }

      /* Are we doing deltas?  If so, we can be more accurate */
      if (total_delta_parts > 0)
        {
          guint64 fetched_delta_part_size =
            ostree_async_progress_get_uint64 (progress,
                                              "fetched-delta-part-size");
          g_autofree char *formatted_fetched = NULL;
          g_autofree char *formatted_total = NULL;

          /* Here we merge together deltaparts + fallbacks to avoid bloating the text UI */
          fetched_delta_parts += fetched_delta_part_fallbacks;
          total_delta_parts += total_delta_part_fallbacks;

          formatted_fetched = g_format_size (fetched_delta_part_size);
          formatted_total = g_format_size (total_delta_part_size);

          if (bytes_sec > 0)
            {
              /* MAX(0, value) here just to be defensive */
              guint64 est_time_remaining =
                MAX (0,
                     (total_delta_part_size -
                      fetched_delta_part_size)) / bytes_sec;
              g_autofree char *formatted_est_time_remaining =
                _formatted_time_remaining_from_seconds (est_time_remaining);
              /* No space between %s and remaining, since formatted_est_time_remaining has a trailing space */
              g_string_append_printf (buf,
                                      "Receiving delta parts: %u/%u %s/%s %s/s %sremaining",
                                      fetched_delta_parts, total_delta_parts,
                                      formatted_fetched, formatted_total,
                                      formatted_bytes_sec,
                                      formatted_est_time_remaining);
            }
          else
            {
              g_string_append_printf (buf,
                                      "Receiving delta parts: %u/%u %s/%s",
                                      fetched_delta_parts, total_delta_parts,
                                      formatted_fetched, formatted_total);
            }
        }
      else if (scanning || outstanding_metadata_fetches)
        {
          g_string_append_printf (buf,
                                  "Receiving metadata objects: %u/(estimating) %s/s %s",
                                  metadata_fetched, formatted_bytes_sec,
                                  formatted_bytes_transferred);
        }
      else
        {
          g_string_append_printf (buf,
                                  "Receiving objects: %u%% (%u/%u) %s/s %s",
                                  (guint) ((((double) fetched) / requested) *
                                           100), fetched, requested,
                                  formatted_bytes_sec,
                                  formatted_bytes_transferred);
        }
    }
  else if (outstanding_writes)
    {
      g_string_append_printf (buf, "Writing objects: %u", outstanding_writes);
    }
  else
    {
      g_string_append_printf (buf, "Scanning metadata: %u",
                              n_scanned_metadata);
    }

  g_message ("Ostree upgrade progress: %s", buf->str);
}

static void
_ostree_upgrader_update_thread (GTask * task,
                                gpointer source,
                                gpointer task_data,
                                GCancellable * cancellable)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (source);
  GError *error = NULL;
  g_autoptr (OstreeSysrootUpgrader) upgrader = NULL;
  g_autoptr (OstreeAsyncProgress) progress = NULL;
  GKeyFile *origin = NULL;
  GMainContext *task_context = NULL;
  gboolean ret;
  gboolean changed = FALSE;
  gboolean deployed = FALSE;

  g_autoptr (OstreeRepo) repo = NULL;
  g_autofree char *origin_refspec = NULL;
  g_autofree char *rev = NULL;
  g_autoptr (GPtrArray) deployments = NULL;
  gboolean blacklisted = FALSE;
  int i;

  task_context = g_main_context_new ();
  g_main_context_push_thread_default (task_context);

  upgrader = ostree_sysroot_upgrader_new (self->sysroot, NULL, &error);
  if (upgrader == NULL)
    {
      g_task_return_error (task, error);
      goto err;
    }

  /* Need to cleanup override-commit field from origin file.
   * In other case it would be used as ref to download */
  origin = ostree_sysroot_upgrader_dup_origin(upgrader);
  if (origin != NULL)
    {
      ostree_deployment_origin_remove_transient_state (origin);
      if (!ostree_sysroot_upgrader_set_origin (upgrader, origin, NULL, &error))
        {
          g_warning ("Unable to clear state of origin for upgrade");
          g_task_return_error (task, error);
          goto err;
        }
    }

  progress =
    ostree_async_progress_new_and_connect (_ostree_upgrader_log_progress,
                                           self);

  /*Download commit's metadata first and check in blacklist */
  if (! ostree_sysroot_upgrader_pull (upgrader,
                                      OSTREE_REPO_PULL_FLAGS_COMMIT_ONLY,
                                      0, progress, &changed,
                                      NULL, &error))
    {
      ostree_sysroot_cleanup (self->sysroot, NULL, NULL);
      g_task_return_error (task, error);
      goto err;
    }

  if (!changed)
    {
      ostree_async_progress_finish (progress);
      goto out;
    }

  /* Ostree considers things to have changed if the last version is
   * different from the booted deployment. Figure out if that last revision
   * was already deployed and if so consider no changes */
  origin_refspec = g_key_file_get_string (origin,
                                          "origin", "refspec",
                                          NULL);
  ret = ostree_sysroot_get_repo (self->sysroot, &repo, NULL, &error);
  if (!ret)
    {
      g_task_return_error (task, error);
      goto err;
    }

  ret =
    ostree_repo_resolve_rev (repo, origin_refspec, FALSE, &rev, &error);
  if (!ret)
    {
      g_task_return_error (task, error);
      goto err;
    }

  /* Check if commit is not blacklisted */
  blacklisted = aum_ostree_upgrader_check_blacklisted(BLACKLIST, rev);
  if (blacklisted)
    {
      ostree_async_progress_finish (progress);
      g_warning ("Revision '%s' is marked as blacklisted; skipping", rev);
      changed = FALSE;
      goto out;
    }

  deployments = ostree_sysroot_get_deployments (self->sysroot);
  for (i = 0; i < deployments->len; i++)
    {
      OstreeDeployment *d = g_ptr_array_index (deployments, i);
      const gchar *csum = ostree_deployment_get_csum (d);

      if (!g_strcmp0 (rev, csum))
        {
          g_message
            ("Latest revision already deployed; just pending reboot");
          deployed = TRUE;
          changed = FALSE;
        }
    }

  /* Download the commit itself */
  ret = ostree_sysroot_upgrader_pull (upgrader, 0, 0,
                                      progress, &changed,
                                      NULL, &error);
  if (!ret)
    {
      ostree_sysroot_cleanup (self->sysroot, NULL, NULL);
      g_task_return_error (task, error);
      goto err;
    }

  ostree_async_progress_finish (progress);

  if (!deployed)
    {
      g_message ("New upgrade downloaded! Deploying..");

      _ostree_upgrader_set_upgrade_state (self,
                                          AUM_OSTREE_UPGRADE_STATE_DEPLOYING);

      ret = ostree_sysroot_upgrader_deploy (upgrader, NULL, &error);
      if (!ret)
        {
          g_task_return_error (task, error);
          goto err;
        }
    }

out:
  g_task_return_int (task,
                     changed ? OSTREE_UPGRADE_READY
                             : OSTREE_UPGRADE_NO_UPDATE);

err:
  g_main_context_pop_thread_default (task_context);
  g_main_context_unref (task_context);
}

static void
_ostree_upgrader_start_update (AumOstreeUpgrader * self)
{
  GTask *task = NULL;

  if (self->updating || self->sysroot == NULL)
    return;

  g_message ("Ostree upgrade poll starting");
  _ostree_upgrader_set_upgrade_state (self,
                                      AUM_OSTREE_UPGRADE_STATE_CHECKING);


  self->updating = TRUE;
  task = g_task_new (self, NULL, _ostree_upgrader_update_cb, NULL);
  g_object_ref (self);
  g_task_run_in_thread (task, _ostree_upgrader_update_thread);
}

void
aum_ostree_upgrader_set_active (AumOstreeUpgrader * self, gboolean active)
{
  if (self->active == active)
    return;

  self->active = active;
  if (active)
    {
      if (self->upgrade_state != AUM_OSTREE_UPGRADE_STATE_PENDING)
        {
          _ostree_upgrader_start_update (self);
        }
    }
  else
    {
      /* On deactivation just simply drop the timer if running, an
       * ongoing update will error out by itself if the network fails
       * for some reason */
      if (self->timeout_id > 0)
        {
          g_source_remove (self->timeout_id);
        }
    }
}

void
aum_ostree_upgrader_set_ready (AumOstreeUpgrader *self)
{
  GSource *source;

  if (self->timeout_id <= 0)
    return;

  source = g_main_context_find_source_by_id (NULL, self->timeout_id);
  if (source == NULL) {
    /* Already destroyed */
    g_warning ("Source ID %u was not found", self->timeout_id);
    return;
  }

  g_source_set_ready_time (source, 0);
}

/**
 * @brief Check if commit is signed
 *
 * Hardcoded only `ed25519` signature check atm.
 *
 * @param repo
 * @param checksum
 * @param cancellable
 * @param error
 *
 * @return TRUE if verification flag is not set in repo config file
 *         TRUE if verification flag is set and valid signature is available
 */
gboolean
aum_check_signature (OstreeRepo *repo,
                     char *checksum,
                     GCancellable *cancellable,
                     GError **error)
{
  gboolean ret = FALSE;
  gboolean check_sign = FALSE;
  const gchar *remote_name = "origin";
  const gchar *signature_name = "ed25519";

  g_autoptr (OstreeSign) sign = NULL;
  g_autofree gchar *pk_ascii = NULL;
  g_autofree gchar *pk_file = NULL;

  ostree_repo_get_remote_boolean_option (repo,
                                         remote_name,
                                         "sign-verify",
                                         FALSE,
                                         &check_sign,
                                         NULL);
  if (!check_sign)
    {
      g_warning ("Signature verification is switched off");
      ret = TRUE;
      goto out;
    }

  if ((sign = ostree_sign_get_by_name (signature_name, error)) == NULL)
    goto out;

  /* Load keys for remote from file if option exists */
  ostree_repo_get_remote_option (repo,
                                 remote_name,
                                 "verification-file",
                                 NULL,
                                 &pk_file,
                                 NULL);
  if (pk_file != NULL)
    {
      g_autoptr (GVariantBuilder) builder = NULL;
      g_autoptr (GVariant) options = NULL;

      builder = g_variant_builder_new (G_VARIANT_TYPE ("a{sv}"));
      g_variant_builder_add (builder, "{sv}", "filename", g_variant_new_string (pk_file));
      options = g_variant_builder_end (builder);

      /* If the filename is explicitly provided in config but it is not in working state -- do not proceed further */
      if (!ostree_sign_load_pk (sign, options, error))
        goto out;
    }

  /* Load key for remote from config if option exists */
  ostree_repo_get_remote_option (repo,
                                 remote_name,
                                 "verification-key", NULL,
                                 &pk_ascii, NULL);
  if (pk_ascii != NULL)
    {
      g_autoptr (GVariant) pk = NULL;

      gsize key_len = 0;
      g_autofree guchar *key = g_base64_decode (pk_ascii, &key_len);
      pk = g_variant_new_fixed_array (G_VARIANT_TYPE_BYTE, key, key_len, sizeof(guchar));

      /* If the key is explicitly set in config but it is not in working state -- do not proceed further */
      if (!ostree_sign_set_pk (sign, pk, error))
        goto out;
    }

  /* Set return to true if any sign fit.
   * If not pre-loaded any key above -- call below will use the system-provided keys */
  ret = ostree_sign_commit_verify (sign,
                                   repo,
                                   checksum,
                                   NULL,
                                   error);

out:
  if (!ret && (error == NULL))
      g_set_error_literal (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                           "Can't verify commit");

  return ret;
}

/**
 * @brief Collect needed info from static delta
 *
 * It is possible that commit doesn't contain any metadata
 * and/or required metadata keys. This function only parse
 * the commit and collect needed information. The analysis
 * should be done in other places.
 *
 * @param[IN] filename -- path to upgrade file
 * @param[OUT] checksum -- checksum of target commit
 * @param[OUT] collection_id -- Collection ID used in commit metadata
 * @param[OUT] refs -- refs found in the commit metadata
 * @param[OUT] timestamp -- timestamp of commit
 * @param error -- contain error if any
 *
 * @return true if inlined commit is correct and could be parsed.
 */
gboolean
aum_get_target_metadata (const gchar *filename,
                         char **checksum,
                         char **out_collection_id,
                         char ***out_refs,
                         guint64 *timestamp,
                         GError **error)
{
  g_autoptr (GMappedFile) map = NULL;
  g_autoptr (GBytes) bytes = NULL;
  g_autoptr (GVariant) superblock = NULL;
  g_autoptr (GVariant) csum_v = NULL;
  g_autoptr (GVariant) commit = NULL;
  g_autoptr (GVariant) metadata = NULL;

  g_return_val_if_fail(*checksum == NULL, FALSE);
  g_return_val_if_fail(*out_collection_id == NULL, FALSE);
  g_return_val_if_fail(*out_refs == NULL, FALSE);

  map = g_mapped_file_new (filename, FALSE, error);
  if (!map)
    goto out;

  bytes = g_mapped_file_get_bytes (map);
  if (!bytes)
    goto out;

  superblock = g_variant_new_from_bytes (G_VARIANT_TYPE(OSTREE_STATIC_DELTA_SUPERBLOCK_FORMAT), bytes, FALSE);
  if (!superblock)
    goto out;

  csum_v = g_variant_get_child_value (superblock, DELTA_CHECKSUM_TO);
  if (!csum_v)
    goto out;

  if (!ostree_validate_structureof_csum_v (csum_v, error))
    goto out;

  *checksum = ostree_checksum_from_bytes_v (csum_v);

  // Parse the inlined commit.
  commit = g_variant_get_child_value (superblock, DELTA_COMMIT);
  if (!ostree_validate_structureof_commit(commit, error))
      goto out;

  // Read needed values from commit's metadata
  metadata = g_variant_get_child_value (commit, COMMIT_METADATA);
  g_message ("Metadata read from commit '%s': %s",
           *checksum,
           g_variant_print(metadata, TRUE));

  *timestamp = ostree_commit_get_timestamp(commit);

  const char *collection_id = NULL;
  /* In case if collection_id is not found, the latter functions will work
   * as the old pre-collection versions */
  if (g_variant_lookup (metadata,
                         OSTREE_COMMIT_META_KEY_COLLECTION_BINDING,
                         "&s",
                         &collection_id))
    {
      if (!ostree_validate_collection_id (collection_id, error))
        {
          g_set_error (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
                       "wrong Collection ID name in file %s",
                       filename);
          goto out;
        }
      *out_collection_id = g_strdup(collection_id);
    }

  // Refs
  char **refs = NULL;
  /* It is possible that commit doesn't have refs */
  if (g_variant_lookup (metadata,
                        OSTREE_COMMIT_META_KEY_REF_BINDING,
                        "^a&s",
                        &refs))
    *out_refs = g_strdupv(refs);

  return TRUE;

out:
  if (error && *error)
    g_prefix_error (error, "Invalid file: ");
  else
    g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED, "Invalid file");

  return FALSE;
}

/**
 * @brief Check if there is any remote with enabled Collection ID
 *
 * Apertis is using only the single remote 'origin', so it is enough to know
 * that this remote is configured to use Collection ID information.
 * Do not care about the Collection ID itself since it would be used automatically
 * during validation of Collection ID provided in update commit.
 *
 * @param repo -- repository
 *
 * @return TRUE if any remote is configured to use Collection ID
 */
gboolean
aum_remote_collection_id_is_set(OstreeRepo *repo)
{
  g_auto(GStrv) remotes = NULL;
  guint n_remotes = 0;
  g_autofree gchar *remote_collection_id = NULL;

  remotes = ostree_repo_remote_list(repo, &n_remotes);

  for (guint i = 0; i < n_remotes; i++)
    {
      // Check if that remote is configured to have collection_id
      // atm we do not care about the name
      if (ostree_repo_get_remote_option (repo,
                                         remotes[i],
                                         "collection-id",
                                         NULL,
                                         &remote_collection_id,
                                         NULL) &&
          remote_collection_id != NULL)
        {
          if (ostree_validate_collection_id (remote_collection_id, NULL))
            g_debug("Remote %s: collection-id=%s", remotes[i], remote_collection_id);

          return TRUE;
        }
    }

  return FALSE;
}

/**
 * @brief Check if Collection refs are applicable for repo
 *
 * Check if Collection ID and at least a single ref name
 * is suitable for local repository.
 *
 * @param repo
 * @param collection_id
 * @param refs
 * @param[OUT] ref_name -- name of applicable ref
 * @param error
 *
 * @return TRUE if Collection ID and any ref are valid,
 *         ref_name is initialised with suitable name.
 */
gboolean
aum_validate_collection_refs (OstreeRepo *repo,
                              char *collection_id,
                              char **refs,
                              char **ref_name,
                              GError **error)
{
  gboolean found = FALSE;

  g_return_val_if_fail(repo != NULL, FALSE);
  g_return_val_if_fail(*ref_name == NULL, FALSE);

  if (collection_id == NULL || refs == NULL)
    goto out;

  for (char **ref = refs; *ref != NULL; ++ref)
    {
      const OstreeCollectionRef collection_ref = { (char *) collection_id, (char *) *ref };

      if (ostree_repo_resolve_collection_ref (repo, &collection_ref,
                                              FALSE,
                                              OSTREE_REPO_RESOLVE_REV_EXT_NONE,
                                              NULL,
                                              NULL,
                                              NULL))
        {
          // Stop as soon as we found any ref
          // FIXME: deal somehow with multiple ref names
          found = TRUE;
          *ref_name = g_strdup(*ref);
          break;
        }
    }

out:
  if (!found)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
                   "Couldn't find applicable ref name for Collection ID (%s)",
                   collection_id);

    }

  return found;
}

gboolean
aum_validate_timestamp(OstreeRepo *repo,
                       OstreeDeployment *booted_deployment,
                       guint64 commit_ts,
                       GError **error)
{
  g_autoptr (GVariant) booted_commit = NULL;
  if (!ostree_repo_load_commit (repo,
                                ostree_deployment_get_csum(booted_deployment),
                                &booted_commit,
                                NULL,
                                error))
    {
      goto out;
    }

  const guint64 booted_ts = ostree_commit_get_timestamp (booted_commit);
  if ( commit_ts <= booted_ts)
    {
      goto out;
    }

  return TRUE;

out:
  return FALSE;
}

void
_ostree_upgrader_static_delta_thread (GTask * task,
                                      gpointer source,
                                      gpointer task_data,
                                      GCancellable *cancellable)
{
  AumOstreeUpgrader *self = AUM_OSTREE_UPGRADER (source);
  GMainContext *task_context = NULL;
  GError* error = NULL;
  g_autoptr (GFile) file = NULL;
  g_autoptr (OstreeRepo) repo = NULL;
  g_autoptr (GPtrArray) deployments = NULL;
  g_autoptr (OstreeDeployment) new_deployment = NULL;
  g_autofree gchar *checksum = NULL;
  g_autofree char *origin_refspec = NULL;
  OstreeDeployment *merge_deployment = NULL;
  OstreeDeployment *booted_deployment = NULL;
  const gchar *osname = NULL;
  GKeyFile *origin = NULL;
  gboolean changed = TRUE;
  gboolean blacklisted = FALSE;
  g_autofree gchar *collection_id = NULL;
  g_autofree gchar *ref_name = NULL;
  g_auto (GStrv) refs = NULL;
  guint64 commit_ts = 0L;

  task_context = g_main_context_new ();
  g_main_context_push_thread_default (task_context);

  booted_deployment = ostree_sysroot_get_booted_deployment (self->sysroot);

  if (!ostree_sysroot_get_repo (self->sysroot, &repo, NULL, &error))
    goto out;

  /* Collect info from upgrade file */
  if (!aum_get_target_metadata (self->static_delta,
                                &checksum,
                                &collection_id,
                                &refs,
                                &commit_ts,
                                &error))
    {
      changed = FALSE;
      goto out;
    }

  /* Chance for the systems without Collection ID used.
   * Allow to proceed with update for them even if there is
   * no collections in update file.
   */
  if (aum_remote_collection_id_is_set(repo))
    {
      /* Check if static delta is applicable for current system */
      if (!aum_validate_collection_refs (repo,
                                         collection_id,
                                         refs,
                                         &ref_name,
                                         &error))
        {
          changed = FALSE;
          goto out;
        }
    }

  g_debug ("Collection ref: %s:%s", collection_id, ref_name);

  /* Check if timestamp of commit is newer than current deployment */
  if (!aum_validate_timestamp (repo,
                               booted_deployment,
                               commit_ts,
                               &error))
    {
      changed = FALSE;
      goto out;
    }

  /* Check if commit is not blacklisted */
  blacklisted = aum_ostree_upgrader_check_blacklisted(BLACKLIST, checksum);
  if (blacklisted)
    {
      g_warning ("Revision '%s' is marked as blacklisted; skipping", checksum);
      changed = FALSE;
      goto out;
    }

  /* Check if we already do not use the commit ID */
  deployments = ostree_sysroot_get_deployments (self->sysroot);
  for (int i = 0; i < deployments->len; i++)
    {
      OstreeDeployment *d = g_ptr_array_index (deployments, i);
      const gchar *csum = ostree_deployment_get_csum (d);

      if (!g_strcmp0 (checksum, csum))
        {
          g_message("Static delta already applied; skipping");
          changed = FALSE;
          goto out;
        }
    }


  /* Prepare a commit */
  if (!ostree_repo_prepare_transaction (repo, NULL, cancellable, &error))
    goto out;

  /* Create a local reference for the checksum */
  if (ref_name == NULL)
    ref_name = g_strdup("master");
  ostree_repo_transaction_set_ref (repo,
                                   NULL,
                                   ref_name,
                                   checksum);
  /* TODO: switch later, as soon as ostree CLI will be ready */
  /* Create a reference based on collection id ref pair detected from static delta. */
  // ostree_repo_transaction_set_collection_ref (repo, &self->collection_ref, self->checksum);

  file = g_file_new_for_path (self->static_delta);
  if (!ostree_repo_static_delta_execute_offline (repo, file, FALSE, cancellable, &error))
    {
      ostree_repo_abort_transaction (repo, cancellable, NULL);
      goto out;
    }

  if (!aum_check_signature (repo,
                            checksum,
                            cancellable,
                            &error))
    {
      g_message("Static delta have no valid signature; aborted");
      ostree_repo_abort_transaction (repo, cancellable, NULL);
      changed = FALSE;
      goto out;
    }

  /* Commit the transaction */
  if (!ostree_repo_commit_transaction (repo, NULL, cancellable, &error))
    goto out;

  osname = ostree_deployment_get_osname (booted_deployment);
  merge_deployment = ostree_sysroot_get_merge_deployment (self->sysroot, osname);
  origin = ostree_deployment_get_origin (merge_deployment);

  /* deploy the commit
   * ostree_sysroot_deploy_tree garbage collects all deployments */

  if (!ostree_sysroot_deploy_tree (self->sysroot, osname,
                                   checksum,
                                   origin,
                                   merge_deployment,
                                   NULL,
                                   &new_deployment,
                                   cancellable, &error))
    goto out;

  /* Update the boot files */
  if (!ostree_sysroot_simple_write_deployment (self->sysroot, osname,
                                               new_deployment,
                                               merge_deployment,
                                               0,
                                               cancellable, &error))
    goto out;

out:
  /* Remove staged temporary directory */
  /* NB: The cleanup call below do the full repository prune, not only temporary directory */
  ostree_sysroot_cleanup (self->sysroot, cancellable, &error);

  if (error)
    g_task_return_error (task, error);
  else
    g_task_return_int (task,
                     changed ? OSTREE_UPGRADE_READY
                             : OSTREE_UPGRADE_NO_UPDATE);

  g_main_context_pop_thread_default (task_context);
  g_main_context_unref (task_context);
}

gboolean
aum_ostree_upgrade_apply_static_delta(AumOstreeUpgrader *self,
                                      const gchar *filename)
{
  GTask *task = NULL;

  if (self->updating || self->sysroot == NULL)
    return FALSE;

  g_message ("Ostree static delta starting");
  _ostree_upgrader_set_upgrade_state (self,
                                      AUM_OSTREE_UPGRADE_STATE_CHECKING);

  self->updating = TRUE;
  self->static_delta = g_strdup (filename);

  task = g_task_new (self, NULL, _ostree_upgrader_update_cb, NULL);
  g_object_ref (self);
  g_task_run_in_thread (task, _ostree_upgrader_static_delta_thread);

  return TRUE;
}

void
aum_ostree_upgrade_apply_pending(AumOstreeUpgrader *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GDBusProxy) systemd_proxy = NULL;

  // Reboot only if update is applied
  if (self->upgrade_state != AUM_OSTREE_UPGRADE_STATE_PENDING)
    return;

  g_debug("Connecting to systemd ...\n");
  systemd_proxy = g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SYSTEM,
                                                 G_DBUS_PROXY_FLAGS_NONE,
                                                 NULL,
                                                 "org.freedesktop.systemd1",
                                                 "/org/freedesktop/systemd1",
                                                 "org.freedesktop.systemd1.Manager",
                                                 NULL,
                                                 &error);
  if (!systemd_proxy)
    goto out;

  g_message ("Rebooting to apply pending update");
  g_dbus_proxy_call_sync (systemd_proxy,
                          "Reboot",
                          NULL,
                          G_DBUS_CALL_FLAGS_NONE,
                          30000,
                          NULL,
                          &error);

out:
  if (error)
    {
      g_warning("Failed to invoke reboot: %s\n", error->message);
    }
}

gboolean
aum_ostree_upgrader_undeploy(AumOstreeUpgrader *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GPtrArray) deployments = NULL;
  g_autoptr (OstreeDeployment) pending_deployment = NULL;

  g_message ("Undeploy requested");

  deployments = ostree_sysroot_get_deployments (self->sysroot);
  ostree_sysroot_query_deployments_for (self->sysroot, NULL, &pending_deployment, NULL);

  if ( pending_deployment == NULL )
    {
      /* FIXME: how to handle situation with rollback 
       * but without pending updates? */
      g_message ("No pending updates");
      goto out;
    }

  /* Add the ID into black list */
  aum_ostree_upgrader_set_blacklisted (BLACKLIST,
                                       ostree_deployment_get_csum(pending_deployment));

  if (!g_ptr_array_remove (deployments, pending_deployment))
    {
      /* FIXME: possible races? */
      g_message ("Failed to remove deployment from the list");
      goto err;
    }

  if (!ostree_sysroot_write_deployments (self->sysroot,
                                         deployments, NULL, &error))
      goto err;

  if (!ostree_sysroot_cleanup (self->sysroot, NULL, &error))
    goto err;

out:
  return TRUE;

err:
  g_message ("Failed to undeploy: %s", error ? error->message : "unknown error");
  return FALSE;
}

/**
 * @brief Check commit ID in blacklist configuration
 *
 * If we can't load the file or get ID from it by any reason
 * we are not able to check if the commit is in list.
 * So permit to use the commit in that case.
 *
 * @param filename
 * @param checksum
 *
 * @return TRUE if blacklisted
 */
gboolean
aum_ostree_upgrader_check_blacklisted(const gchar *filename,
                                      const gchar *checksum)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GKeyFile) blfile = NULL;
  gboolean blacklisted = FALSE;

  blfile = g_key_file_new ();

  if (!g_key_file_load_from_file (blfile, filename, G_KEY_FILE_NONE, &error))
      goto out;

  /* Return FALSE if not found, could not be parsed or set to FALSE externally */
  blacklisted = g_key_file_get_boolean (blfile, "blacklist", checksum, NULL);

out:
  if (error)
    g_warning ("Cannot check the ID in black list: %s", error->message);
  return blacklisted;
}

/**
 * @brief Mark ID as blacklisted
 *
 * @param filename
 * @param checksum
 */
void
aum_ostree_upgrader_set_blacklisted(const gchar *filename,
                                      const gchar *checksum)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GKeyFile) blfile = NULL;

  blfile = g_key_file_new ();

  if (g_file_test(filename, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))
    {
      if (!g_key_file_load_from_file (blfile, filename, G_KEY_FILE_NONE, &error))
        goto out;
    }

  g_key_file_set_boolean (blfile, "blacklist", checksum, TRUE);

  g_key_file_save_to_file (blfile, filename, &error);

out:
  if (error)
    g_warning ("Cannot add the ID into black list: %s", error->message);
}


G_DEFINE_TYPE (AumOstreeUpgrader, aum_ostree_upgrader, G_TYPE_OBJECT)
