/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/**
 * SECTION: bootstate
 * @title: AumBootState
 * @short_description: Ostree upgrade handler
 * @include: boot-state.h
 *
 * The boot state is responsible for reseting the bootcount
 */

#include "boot-state.h"
#include "boot-state-file.h"
#include "boot-state-uboot-env.h"
#include "boot-state-uboot-ext.h"

G_DEFINE_INTERFACE (AumBootState, aum_boot_state, G_TYPE_OBJECT)

static void
aum_boot_state_default_init (AumBootStateInterface *iface)
{
}

gboolean
aum_boot_state_is_active (AumBootState *self)
{
  g_return_val_if_fail (AUM_IS_BOOT_STATE (self), FALSE);

  return AUM_BOOT_STATE_GET_IFACE (self)->is_active (self);
}

const gchar*
aum_boot_state_get_name (AumBootState *self)
{
  g_return_val_if_fail (AUM_IS_BOOT_STATE (self), FALSE);

  return AUM_BOOT_STATE_GET_IFACE (self)->get_name (self);
}

gint
aum_boot_state_get_boot_count (AumBootState *self)
{
  g_return_val_if_fail (AUM_IS_BOOT_STATE (self), -1);
  g_return_val_if_fail (AUM_BOOT_STATE_GET_IFACE (self)->get_boot_count != NULL, -1);

  return AUM_BOOT_STATE_GET_IFACE (self)->get_boot_count (self);
}

gint
aum_boot_state_get_boot_limit (AumBootState *self)
{
  g_return_val_if_fail (AUM_IS_BOOT_STATE (self), -1);
  g_return_val_if_fail (AUM_BOOT_STATE_GET_IFACE (self)->get_boot_limit != NULL, -1);

  return AUM_BOOT_STATE_GET_IFACE (self)->get_boot_limit (self);
}

void
aum_boot_state_set_boot_limit (AumBootState *self, guint limit)
{
  g_return_if_fail (AUM_IS_BOOT_STATE (self));
  g_return_if_fail (AUM_BOOT_STATE_GET_IFACE (self)->set_boot_limit != NULL);

  return AUM_BOOT_STATE_GET_IFACE (self)->set_boot_limit (self, limit);
}

gboolean
aum_boot_state_get_update_available (AumBootState *self)
{
  g_return_val_if_fail (AUM_IS_BOOT_STATE (self), -1);
  g_return_val_if_fail (AUM_BOOT_STATE_GET_IFACE (self)->get_update_available != NULL, -1);

  return AUM_BOOT_STATE_GET_IFACE (self)->get_update_available (self);
}

gboolean
aum_boot_state_set_update_available (AumBootState *self, gboolean update_available)
{
  g_return_val_if_fail (AUM_IS_BOOT_STATE (self), -1);
  g_return_val_if_fail (AUM_BOOT_STATE_GET_IFACE (self)->set_update_available != NULL, -1);

  return AUM_BOOT_STATE_GET_IFACE (self)->set_update_available (self, update_available);
}

gboolean boot_state_check_boot_successful (AumBootState *self)
{
  gint count = 0;
  gint limit = 0;

  g_return_val_if_fail (AUM_IS_BOOT_STATE (self), FALSE);

  count = aum_boot_state_get_boot_count (self);
  limit = aum_boot_state_get_boot_limit (self);

  if (count < limit)
    return TRUE;

  return FALSE;
}

AumBootState *
boot_state_factory_get_active ()
{
  GType subtypes [] = { AUM_TYPE_BOOT_STATE_UBOOT_EXT, AUM_TYPE_BOOT_STATE_UBOOT_ENV, AUM_TYPE_BOOT_STATE_FILE };
  gint i;
  AumBootState * ret = NULL;

  for (i = 0; (ret == NULL) && i < G_N_ELEMENTS(subtypes); i++)
   {
      g_autoptr (AumBootState) boot_state = g_object_new (subtypes [i], NULL);

      if (aum_boot_state_is_active (boot_state))
        {
          g_message ("Using %s backend", aum_boot_state_get_name (boot_state));

          ret = g_steal_pointer (&boot_state);
        }
    }

  return ret;
}

AumBootState *
boot_state_factory_get_by_type (const gchar *name)
{
  GType subtypes [] = { AUM_TYPE_BOOT_STATE_UBOOT_EXT, AUM_TYPE_BOOT_STATE_UBOOT_ENV, AUM_TYPE_BOOT_STATE_FILE };
  gint i;
  AumBootState * ret = NULL;

  for (i = 0; (ret == NULL) && i < G_N_ELEMENTS(subtypes); i++)
   {
      g_autoptr (AumBootState) boot_state = g_object_new (subtypes [i], NULL);

      if (g_str_equal (name, aum_boot_state_get_name (boot_state)))
        {
          g_message ("Using %s backend", aum_boot_state_get_name (boot_state));

          ret = g_steal_pointer (&boot_state);
        }
    }

  return ret;
}
