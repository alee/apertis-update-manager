/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/* inclusion guard */
#ifndef __AUM_BOOT_STATE_H__
#define __AUM_BOOT_STATE_H__

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define AUM_TYPE_BOOT_STATE (aum_boot_state_get_type ())
G_DECLARE_INTERFACE (AumBootState, aum_boot_state, AUM, BOOT_STATE, GObject)

struct _AumBootStateInterface
{
  GTypeInterface g_iface;

  gboolean      (* is_active)            (AumBootState  *self);
  const char *  (* get_name)             (AumBootState  *self);
  gint          (* get_boot_count)       (AumBootState  *self);
  gint          (* get_boot_limit)       (AumBootState  *self);
  void          (* set_boot_limit)       (AumBootState  *self, guint limit);
  gboolean      (* get_update_available) (AumBootState  *self);
  gboolean      (* set_update_available) (AumBootState  *self, gboolean update_available);
};

GType aum_boot_state_get_type (void) G_GNUC_CONST;

AumBootState * boot_state_factory_get_active ();
AumBootState * boot_state_factory_get_by_type (const gchar *name);
gboolean boot_state_check_boot_successful (AumBootState *self);

gboolean aum_boot_state_is_active            (AumBootState *self);
const gchar* aum_boot_state_get_name         (AumBootState *self);
gint aum_boot_state_get_boot_count           (AumBootState *self);
gint aum_boot_state_get_boot_limit           (AumBootState *self);
void aum_boot_state_set_boot_limit           (AumBootState *self, guint limit);
gboolean aum_boot_state_get_update_available (AumBootState *self);
gboolean aum_boot_state_set_update_available (AumBootState *self, gboolean update_available);
gboolean aum_boot_state_update_on_disk       (AumBootState *self);

G_END_DECLS

#endif /* __AUM_BOOT_STATE_H__ */
