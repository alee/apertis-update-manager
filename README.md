Apertis Update Manager
======================

The apertis-update-manager is a component in the OSTree-based images to manage
the update process and to provide a high level D-Bus interface for user
interaction. In this cycle only offline updates over USB Mass Storage devices
have been implemented on the armhf images targeting the i.MX6 Sabrelite platform.
In the coming cycles online OTA updates are planned and support will be extended
to all the other Apertis reference platforms.

The daemon
==========

The daemon is started and stopped using systemd.
The typical boot sequence is similar to the log below:

```
$ systemctl status apertis-update-manager
● apertis-update-manager.service - Apertis update manager
   Loaded: loaded (/lib/systemd/system/apertis-update-manager.service; enabled; 
vendor preset: enabled)
   Active: active (running) since Mon 2018-12-10 13:26:14 UTC; 31min 
ago
 Main PID: 340 (apertis-update-)
   CGroup: /system.slice/apertis-update-manager.service
           └─340 /usr/lib/apertis-update-manager/apertis-update-managerd

Dec 10 13:26:13 apertis systemd[1]: Starting Apertis update manager...
Dec 10 13:26:14 apertis apertis-update-[340]: Could not load configurati
on file: No such file or directory
Dec 10 13:26:14 apertis apertis-update-[340]: Auto update status: disabl
ed
Dec 10 13:26:14 apertis systemd[1]: Started Apertis update manager.
Dec 10 13:26:14 apertis apertis-update-[340]: Loaded boot state: count 0
, limit 4, update_available 0
Dec 10 13:26:14 apertis apertis-update-[340]: Using UBoot environment ba
ckend
Dec 10 13:26:19 apertis apertis-update-[340]: Network status: online
```

It is run at boot from a dedicated service file requested by `multi-user.target`.

Command line HMI
================

A command line HMI is available.

```
$ updatectl -h
Usage:
  updatectl [OPTION...] - Update manager tool

Help Options:
  -h, --help                         Show help options

Application Options:
  -r, --register-upgrade-handler     Register a system upgrade handler
  -m, --mark-boot-successful         Mark the boot successful
  -d, --apply-static-delta           Apply a static delta file
```

Monitoring apertis-update-manager
=================================

It is possible to monitor status of update manager from a remote process:

An example implementation is available in `updatectl`:

```
updatectl --register-upgrade-handler
```

If an upgrade handler is registered, the apertis-update-manager will not reboot
after an update. In other case the apertis-update-manager will automatically
reboot after an update.

The following events will be displayed: 
* Connectivity status change
* Availability of new network updates
* Progress of updates

The DBus API can be introspected using the command
```
$ gdbus introspect -y -d org.apertis.ApertisUpdateManager -o / -r
```

Mark the update successful
========================

```
updatectl --mark-update-successful
```

Asks apertis-update-manager to check the boot count.
If the boot count is higher or equal to the boot limit, this means U-Boot have
triggered a rollback, and the failing release will be undeployed.
If the boot count is lower than the boot limit, this means the current boot was
successful after an update. In this case, the update will be marked
as successful and appropriate variables will be reset.

It is run at boot time from a dedicated service file triggered after `default.target`.

If the system is not in any update state -- just do nothing.

Automatically network updates
================================

Network updates are disabled by default. They can be enabled in the configuration files `/etc/apertis-update-manager.ini`.

```
[Manager]
UpdateFromNetwork=true
```

In this case, the apertis-update-manager will poll the server every five minutes when the connectivity is available.

Alternatively, it is possible to enable network update using DBus:
```
$ gdbus call -y -d org.apertis.ApertisUpdateManager -o / -m org.freedesktop.DBus.Properties.Set org.apertis.ApertisUpdateManager UpdatesFromNetwork "<true>"
```

To enable network update and/or start server polling:
```
updatectl --check-network-updates
```

Applying a static update
================================

First get a static delta file,

```
updatectl --apply-static-delta <path to static delta>
```
The update is asynchronous. If updatectl returns TRUE, it's because the update was started properly
but it does not mean it was terminated. The final status can be checked by either register for notifications
or periodically checking `sudo ostree admin status`.

Automatically applying an update from mass storage
==================================================

* Prestwood must be running. Prestwood is only available on target images. We are studying the possiblity to use udisks2 (systemctl status udisks2, udisksctl status).
* A static delta file must be installed on the root of the USB key. It must be named: `static-update.bundle`.
* Plug in the USB key, the apertis-update-manager should be notified.
* If the update manager detects the USB key, a trace "mount added" will be visible in the logs.

Getting logs
============
```
$ journalctl --unit apertis-update-manager
-- Logs begin at Mon 2018-12-10 13:26:10 UTC, end at Mon 2018-12-10 13:46:27 UTC
. --
Dec 10 13:26:13 apertis systemd[1]: Starting Apertis update manager...
Dec 10 13:26:14 apertis apertis-update-[340]: Could not load configurati
on file: No such file or directory
Dec 10 13:26:14 apertis apertis-update-[340]: Auto update status: disabl
ed
Dec 10 13:26:14 apertis systemd[1]: Started Apertis update manager.
Dec 10 13:26:14 apertis apertis-update-[340]: Loaded boot state: count 0
, limit 4, update_available 0
Dec 10 13:26:14 apertis apertis-update-[340]: Using UBoot environment ba
ckend
Dec 10 13:26:19 apertis apertis-update-[340]: Network status: online
```

Backends
========

To check current status of backend used by apertis-update manager and variables
related to upgrade/rollback. It is possible to use `envread` tool, for example:
```
# envread 
** Message: Loaded boot state: count 0, limit 3, update_available 0
** Message: Using UBoot environment backend
** Message: Initial state: 'bootcount' = 0
** Message: Initial state: 'bootlimit' = 3
** Message: Initial state: 'upgrade_available' = 0
```

Uboot environment backend
=========================

The uboot environment backend is automatically selected by the apertis-update-manager
if a valid uboot environment can be read from /dev/mmcblk0 device.
At the time of writing, if the uboot environment is invalid, it is not selected.
It is thus up to the user to active his environment using the commands below from U-boot console:

```
env default -a
saveenv
```

Additionally you may want to use command to set the rollback limit:
```
setenv bootlimit 4
```

File environment backend
========================

This is a DEBUG feature.

```
user@apertis:~/apertis-update-manager/managerd$ cat /etc/boot-state.ini 
[Boot]
Count=0
Limit=4
UpdateAvailable=false
Status=Successful
```

