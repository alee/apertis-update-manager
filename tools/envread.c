/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

#include "../managerd/boot-state-uboot-env.h"

void
print_vars( AumBootState *boot_state, const gchar *message)
{
  gint count = aum_boot_state_get_boot_count(boot_state);
  g_message("%s: 'bootcount' = %d", message, count);

  gint limit = aum_boot_state_get_boot_limit(boot_state);
  g_message("%s: 'bootlimit' = %d", message, limit);

  gint update_available = aum_boot_state_get_update_available(boot_state);
  g_message("%s: 'upgrade_available' = %d", message, update_available);

}

int
main()
{
  g_autoptr (AumBootState) boot_state;
  boot_state = boot_state_factory_get_active();

  print_vars( boot_state, "Initial state");

  /*
  aum_boot_state_set_update_available (boot_state, FALSE);
  print_vars( boot_state, "Mark Successful");

  aum_boot_state_set_update_available (boot_state, TRUE);
  print_vars( boot_state, "Mark update available");
  */
  return 0;
}

