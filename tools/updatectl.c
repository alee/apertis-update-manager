/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

#include <stdlib.h>

#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>

#include "apertis-update-manager-dbus.h"
#include "../managerd/ostree-upgrader.h"

GMainLoop *loop;
aumApertisUpdateManager *manager;

static gboolean register_handler = FALSE;
static gboolean mark_update_successful = FALSE;
static gchar *static_delta_filename = NULL;
static gboolean check_network_updates = FALSE;

static GOptionEntry entries[] =
{
    { "register-upgrade-handler", 'r',
      0, G_OPTION_ARG_NONE, &register_handler,
      "Register a system upgrade handler", NULL },
    { "mark-update-successful", 'm',
      G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, &mark_update_successful,
      "Mark the update successful", NULL },
    { "apply-static-delta", 'd',
      G_OPTION_FLAG_NONE, G_OPTION_ARG_FILENAME, &static_delta_filename,
      "Apply a static delta file", NULL },
    { "check-network-updates", 'u',
      G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, &check_network_updates,
      "Enable network updates and check the availability of new update", NULL },
    { NULL }
};


static void
network_connection_changed (aumApertisUpdateManager *manager)
{
  gboolean connected;
  g_object_get (manager, "network-connected", &connected, NULL);
  g_message ("Network connected: %s", connected ? "Yes" : "No");
}

static void
upgrade_state_changed (aumApertisUpdateManager *manager)
{
  uint upgradestate;
  gchar *statuses[] = { "Unknown",
                        "Checking",
                        "Downloading",
                        "Deploying" };

  g_object_get (manager, "system-upgrade-state", &upgradestate, NULL);

  /* Hard-copy the values from the dbus spec; A real client should probably
   * just copy across the enum though
   */
  if (upgradestate < G_N_ELEMENTS(statuses))
    {
      g_message ("Upgrade status: %s", statuses[upgradestate]);
    }
  else if (upgradestate == AUM_OSTREE_UPGRADE_STATE_PENDING)
    {
      g_message ("An upgrade is pending");
    }
  else if (upgradestate == AUM_OSTREE_UPGRADE_STATE_UPTODATE)
    {
      g_message ("System is up to date");
    }
}

static void
properties_changed (GDBusProxy *proxy,
                    GVariant   *properties,
                    const gchar * const *invalidated_properties,
                    gpointer    user_data)
{

  g_autoptr (GVariantDict) dict = g_variant_dict_new (properties);

  if (g_variant_dict_contains (dict, "NetworkConnected")
      || g_strv_contains (invalidated_properties, "NetworkConnected"))
    network_connection_changed (manager);

  if (g_variant_dict_contains (dict, "SystemUpgradeState")
      || g_strv_contains (invalidated_properties, "SystemUpgradeState"))
    upgrade_state_changed (manager);
}

static void
upgrade_handler_registered (GObject *source,
                            GAsyncResult *result,
                            gpointer user_data)
{
 g_autoptr (GError) error = NULL;

 if (!aum_apertis_update_manager_call_register_system_upgrade_handler_finish (AUM_APERTIS_UPDATE_MANAGER (source),
                                                                          result,
                                                                          &error))
   {
     g_error ("Failed to register system upgrade handler: %s", error->message);
     g_main_loop_quit (loop);
     return;
   }
}

static void
name_owner_cb (GObject    *gobject,
               GParamSpec *pspec,
               gpointer    user_data)
{
  g_autofree gchar *owner = NULL;

  g_object_get (gobject, "g-name-owner", &owner, NULL);

  if (owner != NULL && register_handler)
    {
      aum_apertis_update_manager_call_register_system_upgrade_handler (manager,
                                                                   NULL,
                                                                   upgrade_handler_registered,
                                                                   NULL);
    }
}

static void
mark_update_successful_callback (GObject *source,
                               GAsyncResult *result,
                               gpointer user_data)
{
 g_autoptr (GError) error = NULL;

 if (!aum_apertis_update_manager_call_mark_update_successful_finish (AUM_APERTIS_UPDATE_MANAGER (source),
                                                                     result,
                                                                     &error))
   g_error ("Failed to mark update successful: %s", error->message);

 g_main_loop_quit (loop);
}

static void
apply_static_delta_handler (GObject *source,
                            GAsyncResult *result,
                            gpointer user_data)
{
 g_autoptr (GError) error = NULL;

 if (!aum_apertis_update_manager_call_apply_static_delta_finish (AUM_APERTIS_UPDATE_MANAGER (source),
                                                                          result,
                                                                          &error))
   {
     g_error ("Failed to apply static delta: %s", error->message);
     g_main_loop_quit (loop);
     return;
   }

  g_main_loop_quit (loop);
}

static void
check_network_updates_callback (GObject *source,
                               GAsyncResult *result,
                               gpointer user_data)
{
 g_autoptr (GError) error = NULL;

 if (!aum_apertis_update_manager_call_check_network_updates_finish (AUM_APERTIS_UPDATE_MANAGER (source),
                                                                    result,
                                                                    &error))
   g_error ("Failed to enable network updates: %s", error->message);

 g_main_loop_quit (loop);
}

static void
got_manager_proxy (GObject *source,
                   GAsyncResult *result,
                   gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  manager = aum_apertis_update_manager_proxy_new_for_bus_finish (result, &error);

  if (manager == NULL)
    {
      g_error ("Failed to get manager proxy: %s", error->message);
      g_main_loop_quit (loop);
      return;
    }

  if (static_delta_filename)
    {
      aum_apertis_update_manager_call_apply_static_delta (manager,
                                                          static_delta_filename,
                                                          NULL,
                                                          apply_static_delta_handler,
                                                          NULL);
    }

  g_signal_connect (manager,
                   "g-properties-changed",
                   G_CALLBACK (properties_changed),
                   NULL);

  network_connection_changed (manager);

  g_signal_connect (manager, "notify::g-name-owner",
                    G_CALLBACK(name_owner_cb),
                    NULL);

  if (register_handler)
    {
      aum_apertis_update_manager_call_register_system_upgrade_handler (manager,
                                                                   NULL,
                                                                   upgrade_handler_registered,
                                                                   NULL);
    }

  if (mark_update_successful)
    {
      aum_apertis_update_manager_call_mark_update_successful (manager,
                                                            NULL,
                                                            mark_update_successful_callback,
                                                            NULL);
    }

  if (check_network_updates)
    {
      aum_apertis_update_manager_call_check_network_updates (manager,
                                                            NULL,
                                                            check_network_updates_callback,
                                                            NULL);
    }
}

int
main (int argc, char **argv)
{
  GOptionContext *context;
  GError *error = NULL;

  context = g_option_context_new ("- Update manager tool");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_print ("Option parsing failed: %s\n", error->message);
      exit (1);
    }

  loop = g_main_loop_new (NULL, FALSE);

  aum_apertis_update_manager_proxy_new_for_bus (G_BUS_TYPE_SYSTEM,
                                            G_DBUS_PROXY_FLAGS_NONE,
                                            "org.apertis.ApertisUpdateManager",
                                            "/",
                                            NULL,
                                            got_manager_proxy,
                                            NULL);

  g_main_loop_run (loop);

  return 0;
}
